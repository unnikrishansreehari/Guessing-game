import random as r

num = r.randrange(10)
guess = 5

while guess >= 0:
    yourguess = int(input("enter your guess "))

    def check(x):
        if yourguess == x:
            print("you win")
            exit()
        elif yourguess > x:
            print("you are close keep trying lower ")
        else:
            print("you are close keep trying higher ")

    if guess > 1:
        check(num)
    elif guess == 1:
        check(num)
        print("this is your last chance")
else:
    print("you lost")
    exit()

guess = guess - 1
